#include <iostream>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

// Prototype de la fnc de lancer
int Throw();

struct TemplateCharacter{
    string name;
    int Victories;
    int Pv;
}Pile, Face;

int CurrentThrow = 0;

int main()
{

    srand(time(NULL));                  // init du random

    // init des variables
    Pile.Pv = 3;                        // 3 Pv pour chaque joueur
    Pile.Victories = 0;                 // Cp de victoires
    Pile.name = "Joueur Pile";          // Init du nom

    Face.Pv = 3;
    Face.Victories = 0;
    Face.name = "Joueur Face";          // Init du nom

    int i = 0;
    for(i=0; i<6; i++){

        CurrentThrow =  Throw();        // D�finis soit 1 ou 0 ==> Pile ou face

        switch(CurrentThrow){
        case 0:
            cout << Pile.name << " Gagne la manche" << endl;
            Pile.Victories++;
            Face.Pv--;
            break;
        case 1:
            cout << Face.name << " Gagne la manche" << endl;
            Face.Victories++;
            Pile.Pv--;
            break;
        }
        if(Pile.Pv == 0 || Face.Pv ==0){
            break;
        }
    }
    if(Pile.Pv ==0){
        cout << endl << Face.name << " Win la partie " << endl;
    }else if(Face.Pv == 0){
        cout << endl << Pile.name << " Win la partie" << endl;
    }
    return 0;
}

int Throw(){
    return (rand()%2);
}
